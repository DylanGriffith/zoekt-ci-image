FROM golang:1.19

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y universal-ctags git

RUN git clone https://github.com/DylanGriffith/zoekt.git
RUN cd /usr/src/app/zoekt && git checkout indexserver-api && go install github.com/sourcegraph/zoekt/cmd/zoekt-indexserver && go install github.com/sourcegraph/zoekt/cmd/zoekt-git-clone && go install github.com/sourcegraph/zoekt/cmd/zoekt-git-index
RUN cd /usr/src/app/zoekt && git checkout search-api-with-reposbranches-filter && go install github.com/sourcegraph/zoekt/cmd/zoekt-webserver

COPY run-zoekt-servers run-zoekt-servers

EXPOSE 6060
EXPOSE 6070

CMD ["sh", "run-zoekt-servers"]
